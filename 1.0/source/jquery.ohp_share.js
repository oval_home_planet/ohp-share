(function($) {
    $.fn.ohp_share = function(options){
        var defaults = {
        	url     : location.href,
        	title   : document.title,
        	sns     : ['facebook','twitter','line','google','hatebu','pocket'],
        	onclick : 'javascript:window.open(this.href, \'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600\');'
        };
        var setting = $.extend(defaults, options);

//-------------------------------

		var self = $(this);

		var snsLink = [];
		var i  = 0;
		var li = '';

		setting.url   = encodeURI(setting.url);
		setting.title = encodeURI(setting.title);

//-------------------------------

		$(window).on('load', function(){
			setButtons();
		});

//-------------------------------

		function setButtons(){
			snsLink['facebook'] = '//www.facebook.com/sharer.php?src=bm&u='+setting.url+'&t='+setting.title;
			snsLink['twitter']  = '//twitter.com/intent/tweet?url='+setting.url+'&text='+setting.title+'&tw_p=tweetbutton';
			snsLink['line']     = '//line.me/R/msg/text/?'+setting.url;
			snsLink['hatebu']   = '//b.hatena.ne.jp/entry/'+setting.url;
			snsLink['pocket']   = '//getpocket.com/edit?url='+setting.url;
			snsLink['google']  = '//plus.google.com/share?url='+setting.url;

			while (i < setting.sns.length) {
				var snsName = setting.sns[i];
				li = li+'<li class="'+snsName+'"><a href="'+snsLink[snsName]+'" onclick="'+setting.onclick+'return false;">'+snsName+'</a></li>';
				i++;
			}

			self.html('<ul class="ohp-share"></ul>');
			self.children('ul.ohp-share').html(li);
		}

//-------------------------------

        return(this);
    };
})(jQuery);