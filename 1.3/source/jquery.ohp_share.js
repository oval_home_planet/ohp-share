(function($) {
    $.fn.ohp_share = function(options){
        var defaults = {
        	url     : location.href,
        	title   : document.title,
        	sns     : ['facebook','twitter','line','hatebu','linkedin','pocket'],
        	onclick : 'javascript:window.open(this.href, \'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600\');'
        };
        var setting = $.extend(defaults, options);

//-------------------------------

		var self = $(this);

		var snsLink = [];
		var i  = 0;
		var li = '';

		setting.url   = encodeURI(setting.url);
		setting.title = encodeURI(setting.title);

//-------------------------------

		setButtons();

//-------------------------------

		function setButtons(){
			snsLink['facebook'] = 'https://www.facebook.com/sharer/sharer.php?u='+setting.url+'&t='+setting.title;
			snsLink['twitter']  = 'https://twitter.com/share?url='+setting.url+'&text='+setting.title;
			snsLink['line']     = 'https://line.me/R/msg/text/?'+setting.url;
			snsLink['hatebu']   = 'https://b.hatena.ne.jp/entry/'+setting.url;
			snsLink['pocket']   = 'https://getpocket.com/edit?url='+setting.url;
            snsLink['linkedin'] = 'https://www.linkedin.com/shareArticle?mini=true&url='+setting.url;
            snsLink['weibo']    = 'http://service.weibo.com/share/share.php?url='+setting.url+'&appkey=97000680&title='+setting.title;
			snsLink['google']   = 'https://plus.google.com/share?url='+setting.url;

			while (i < setting.sns.length) {
				var snsName = setting.sns[i];
				li = li+'<li class="'+snsName+'"><a href="'+snsLink[snsName]+'" onclick="'+setting.onclick+'return false;">'+snsName+'</a></li>';
				i++;
			}

			self.html('<ul class="ohp-share"></ul>');
			self.children('ul.ohp-share').html(li);
		}

//-------------------------------

        return this;
    };
})(jQuery);